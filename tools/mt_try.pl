#!/usr/bin/env perl

use warnings;
use strict;
use diagnostics;

use Path::Class;
use Getopt::Long qw(:config no_ignore_case);
use Data::Printer;

my %options;
my %dict;

GetOptions (
  \%options,
  'dict=s',
  'output=s',
);

# Usage  ./mt_try.py <path to beep file>

unless (defined $options{dict} and -e $options{dict}) {
  print "Could not find dictionary file\n";
  exit 1;
}

print "Loading Beep file... ";
my $dict_handle = file($options{dict})->open('r')
  or die "Can't read $options{dict}: $!";

while (<$dict_handle>) {
  next if /^#/;
  next unless /^\w+\t\w+/;
  chomp;
  my @tokens = split /\t/, $_;
  my $lexeme = uc shift @tokens;
  $dict{$lexeme} = join ' ', @tokens;
}
print "done!\n";

if (@ARGV) {
  print "Processing ", scalar @ARGV, " files\n";

# # if there isn't an output folder, make one
#
# if not os.path.exists(output_folder):
#     os.mkdir(output_folder)

  foreach my $filename (@ARGV) {
    unless (-e $filename) {
      warn "Could not read $filename. Skipping...\n";
      next;
    }

    my $file = file($filename);

#     txtfile = os.path.join(input_folder, file)
#     name, extension = os.path.splitext(file)
#     labfile = os.path.join(output_folder, name + '.lab' )
#     dictfile = os.path.join(output_folder, name + '.dict' )
#
#     print('\nLoading ' + txtfile)
#     print('Creating ' + labfile)
#     print('Creating ' + dictfile)

# # then in the .txt files, throw away the stuff at the beginning, i.e. " and end of the line, i.e. " and ;
# # we just ignore numbers by only working with the 2nd token (the first two entries are numberlines separated by a tab)
# # all words get changed to uppercase, and we put each word on a separate line.  this will be saved as a .lab file (i can't see where?)

#     with open(txtfile, 'r') as file_in, open(labfile, 'w') as labfile_out, open(dictfile, 'w') as dictfile_out:
#         for i, line in enumerate(file_in):
#
#             line = line.strip().upper()
#
#             tokens = line.split()
#             line = ' '.join(tokens[2:])
#
#             if line.startswith('"'):
#                 line = line[1:]
#             if line.endswith(';'):
#                 line = line[:-1]
#             if line.endswith('"'):
#                 line = line[:-1]
#
# # doing more cleanup to the lab file.  making sure words starting/ending with <> brackets also have an opening/closing one
#
#             tokens = line.split()
#             for token in tokens:
#
#                 if token.startswith('<') and not token.endswith('>'):
#                     token = token + '>'
#                 if token.endswith('>') and not token.startswith('<'):
#                     token = '<' + token
#
# # cleanup: if there's a PSIL, replace it with a SILP
#
#                 if token == 'PSIL':
#                     token = 'SILP'
#
# # changing <> and - to corresponding numbers
#
#                 new_token = token.replace('<', '1')
#                 new_token = new_token.replace('>', '2')
#                 new_token = new_token.replace('-', '3')
#
# # making sure .lab file doesn't start with a real word, but with a phonetic sil value.  hence we add 1GA2 at end/beginning if there isn't one of the
# #symbols which gets sil in beep
#
#                 if i == 0:
#                     if not (token.startswith('<') or token.endswith('-') or token == 'SIL' or token == 'SILP' or token == 'PSIL'):
#                         labfile_out.write('1GA2\n')
#
#                 # In case we have an isolated '-'
#                 if not token == '-':
#                     labfile_out.write(new_token + '\n')
#
# # creating the .dict file. checking if a word is in beep.  if a word starts with <, add it on a line in the .dict file followed by a tab and sil
# # if the word doesn't start with a <, put it in dict followed by a tab and XXX
#
#
#                     if not new_token in beep_lexicon:
#                         if token.startswith('<') or token.endswith('-'):
#                             new_token += '\tsil'
#                         else:
#                             new_token += '\tXXX'
#                         dictfile_out.write(new_token + '\n')
#                     else:
#                         # Store the new_token (+phonemes) in the mini_beep_lexicon
#                         mini_beep_lexicon[new_token] = beep_lexicon[new_token]
# # ensuring .lab file ends with a sil value again
#
#         if not (token.startswith('<') or token.endswith('-') or token == 'SIL' or token == 'SILP' or token == 'PSIL'):
#             labfile_out.write('1GA2\n')
#
  }
}
else {
  print "Not enough arguments\n";
  exit(1);
}

print "Done\n";

# # Save the mini_beep_lexicon
# with open('minibeeplexicon.lex', 'w') as file_out:
#     for key in sorted(mini_beep_lexicon.keys()):
#         file_out.write(key + '\t' + mini_beep_lexicon[key] + '\n')
