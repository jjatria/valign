#!/usr/bin/perl

use strict;
use diagnostics;
use warnings;

while (<>) {
	chomp;
	if (/\b((S?HE)(S)|(THEY|YOU)(RE)|(I)(M))\b/) {
		print "$2'$3\n" if ($2);
		print "$4'$5\n" if ($4);
		print "$6'$7\n" if ($6);
	} else {
		print "$_\n";
	}
}
